FROM openjdk:17-alpine

ENV TZ=Europe/Moscow

COPY /target/mybatis*.jar /mybatis.jar

ENTRYPOINT [ "java", 				 \
             "-Duser.timezone=$TZ",  \
             "-jar",           		 \
             "/mybatis.jar"          \
]