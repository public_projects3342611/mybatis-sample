package ru.nazarenko.mybatisrest.service;

public class RegionsDirectoryServiceException extends Exception {
    public RegionsDirectoryServiceException(String message) {
        super(message);
    }
}
