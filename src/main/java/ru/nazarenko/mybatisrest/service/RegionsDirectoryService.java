package ru.nazarenko.mybatisrest.service;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import ru.nazarenko.mybatisrest.model.Region;
import ru.nazarenko.mybatisrest.model.RegionMapper;

import java.util.List;

@Service
public class RegionsDirectoryService {

    private final RegionMapper regionMapper;

    public RegionsDirectoryService(RegionMapper regionMapper) {
        this.regionMapper = regionMapper;
    }

    public List<Region> getAllArticles() {
        return regionMapper.getAllRegions();
    }

    @Cacheable("regions")
    public Region getArticleById(String id) {
        System.out.print("getting region");

        return regionMapper.getRegion(id);
    }

    public void updateArticleByid(String id, Region regionToUpdate) throws RegionsDirectoryServiceException {
        boolean result = regionMapper.updateRegion(id, regionToUpdate.getFullName(), regionToUpdate.getShortName());
        if (result == Boolean.FALSE) {
            throw new RegionsDirectoryServiceException("""
                    Ошибка при обновлении записи
                    id: [ %s ]
                    """.formatted(id));
        }
    }

    public void deleteRegion(String regionid) throws RegionsDirectoryServiceException {
        boolean result = regionMapper.deleteRegion(regionid);
        if (result == Boolean.FALSE) {
            throw new RegionsDirectoryServiceException("""
                    Ошибка при удалении записи
                    id: [ %s ]
                    """.formatted(regionid));
        }
    }

    public void addNewRegion(Region regionToAdd) throws RegionsDirectoryServiceException {
        boolean result = regionMapper.addNewRegion(regionToAdd.getFullName(), regionToAdd.getShortName());

        if (result == Boolean.FALSE) {
            throw new RegionsDirectoryServiceException("""
                    Ошибка при добавлении записи
                    Region: [ %s ]
                    """.formatted(regionToAdd));
        }

    }
}
