package ru.nazarenko.mybatisrest.model;

import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface RegionMapper {
    @Results(id = "regionResultMap", value = {
            @Result(property = "id", column = "region_id"),
            @Result(property = "fullName", column = "full_name"),
            @Result(property = "shortName", column = "short_name")
    })
    @Select("SELECT region_id, full_name, short_name FROM REGION WHERE region_id = #{id}")
    Region getRegion(@Param("id") String id);

    @ResultMap("regionResultMap")
    @Select("SELECT * FROM REGION")
    List<Region> getAllRegions();

    @Update("UPDATE REGION SET full_name = #{newFullName}, short_name = #{newShortName} WHERE region_id = #{id}")
    boolean updateRegion(@Param("id") String id,
                     @Param("newFullName") String fullName,
                     @Param("newShortName") String shortName
    );

    @Delete("DELETE FROM REGION WHERE region_id = #{id}")
    boolean deleteRegion(@Param("id") String id);


    @Insert("INSERT INTO REGION(full_name, short_name) VALUES(#{fullname}, #{shortName})")
    boolean addNewRegion(@Param("fullname") String fullName, @Param("shortName") String shortName);
}
