package ru.nazarenko.mybatisrest.model;

import lombok.Data;

@Data
public class Region {
    private Long id;
    private String fullName;
    private String shortName;

    public Region(String fullName, String shortName) {
        this.fullName = fullName;
        this.shortName = shortName;
    }

    public Region(Long id, String fullName, String shortName) {
        this.id = id;
        this.fullName = fullName;
        this.shortName = shortName;
    }

    public Region() {
    }
}