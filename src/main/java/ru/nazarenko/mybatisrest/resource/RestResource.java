package ru.nazarenko.mybatisrest.resource;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.nazarenko.mybatisrest.model.Region;
import ru.nazarenko.mybatisrest.service.RegionsDirectoryService;
import ru.nazarenko.mybatisrest.service.RegionsDirectoryServiceException;

import java.util.List;

@RestController
@RequestMapping("/api/regions")
public record RestResource(RegionsDirectoryService service) {

    @GetMapping("/all")
    public List<?> getALlRegions() {
        return service.getAllArticles();
    }

    @GetMapping("/{id}")
    public Region getRegion(@PathVariable("id") String regionid) {
        return service.getArticleById(regionid);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<?> updateRegionBy(@PathVariable("id") String regionid,
                                            @RequestBody Region region) {
        try {
            service.updateArticleByid(regionid, region);

            return new ResponseEntity<>(HttpStatus.OK);
        } catch (RegionsDirectoryServiceException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteRegion(@PathVariable("id") String regionid) {
        try {
            service.deleteRegion(regionid);

            return new ResponseEntity<>(HttpStatus.OK);
        } catch (RegionsDirectoryServiceException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
    }

    @PostMapping("/add")
    public ResponseEntity<?> addRegion(@RequestBody Region regionToAdd){
        try {
            service.addNewRegion(regionToAdd);

            return new ResponseEntity<>(HttpStatus.OK);
        } catch (RegionsDirectoryServiceException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
    }
}
